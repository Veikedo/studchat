using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Chat.Dal.Migrations;

namespace Chat.Dal
{
    public interface IAppDbContext : IDisposable
    {
        IDbSet<User> Users { get; }
        IDbSet<HubConnection> HubConnections { get; }
        IDbSet<Conversation> Conversations { get; }
        int SaveChanges();
        Task<int> SaveChangesAsync();
        void MarkAsModified<T>(T entry) where T : class;
    }

    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext() : base("DefaultConnection")
        {
            var strategy = new MigrateDatabaseToLatestVersion<AppDbContext, Configuration>("DefaultConnection");
            Database.SetInitializer(strategy);
        }

        public virtual IDbSet<User> Users { get; set; }
        public virtual IDbSet<HubConnection> HubConnections { get; set; }
        public virtual IDbSet<Conversation> Conversations { get; set; }

        public void MarkAsModified<T>(T entry) where T : class
        {
            Entry(entry).State = EntityState.Modified;
        }
    }
}