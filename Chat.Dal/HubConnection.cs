using System;
using System.ComponentModel.DataAnnotations;

namespace Chat.Dal
{
	public class HubConnection
	{
		[Key]
		public string ConnectionId { get; set; }
		public Guid UserId { get; set; }
		public virtual User User { get; set; }
		public bool IsConnected { get; set; }
	}
}