﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chat.Dal
{
	public class User
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid Id { get; set; }

		public virtual ICollection<HubConnection> HubConnections { get; set; } = new Collection<HubConnection>();
		public virtual Conversation Conversation { get; set; }
		public Guid? ConversationId { get; set; }
		public string UserName { get; set; }
	}
}