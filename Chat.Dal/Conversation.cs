﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Chat.Dal
{
	public class Conversation
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public Guid ConversationId { get; set; }

		public virtual ICollection<User> Users { get; set; } = new Collection<User>();
		public bool IsAlive { get; set; }
		public bool CanConnect { get; set; }
	}
}