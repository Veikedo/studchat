﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Chat.WebTest1.Startup))]
namespace Chat.WebTest1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
