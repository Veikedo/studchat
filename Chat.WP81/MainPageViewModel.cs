﻿using System;
using System.Reactive.Linq;
using Chat.WP81.Helpers;
using ReactiveUI;

namespace Chat.WP81
{
	public class MessageViewModel
	{
		public MessageViewModel(string content)
		{
			Content = content;
		}

		public string Content { get; }
	}

	public class MainPageViewModel : ReactiveObject, IActivatable
	{
		private readonly IChatHub _chatHub;
		private Guid? _dialogId;
		private string _message;
		private ReactiveList<MessageViewModel> _messages;

		public MainPageViewModel()
		{
			_messages = new ReactiveList<MessageViewModel>();

			_chatHub = new ChatHub();

			Send = ReactiveCommand.CreateAsyncTask(
				this.WhenAnyValue(x => x.Message, x => x.DialogId, (m, d) => !m.IsNullOrEmpty() && d != null),
				_ => _chatHub.SendMessageAsync(_dialogId.Value, Message)
				);

			StartNewConversation = ReactiveCommand.CreateAsyncTask(
				_ => _chatHub.StartDialogAsync()
				);

			_chatHub.OnDialogStarted.ObserveOnDispatcher()
				.Subscribe(c =>
				{
					DialogId = c.ConversationId;
					AddMessage("dialog started");
				});

			_chatHub.OnDialogDied.Subscribe(c => DialogId = null);
			_chatHub.OnMessageReceived.ObserveOnDispatcher().Subscribe(c => AddMessage(c.Content));

			_chatHub.StartDialogAsync();
		}

		public Guid? DialogId
		{
			get { return _dialogId; }
			set { this.RaiseAndSetIfChanged(ref _dialogId, value); }
		}

		public ReactiveList<MessageViewModel> Messages
		{
			get { return _messages; }
			set { this.RaiseAndSetIfChanged(ref _messages, value); }
		}

		public string Message
		{
			get { return _message; }
			set { this.RaiseAndSetIfChanged(ref _message, value); }
		}

		public IReactiveCommand Send { get; }
		public IReactiveCommand StartNewConversation { get; }

		private void AddMessage(string text)
		{
			var message = new MessageViewModel(text);
			Messages.Add(message);
		}
	}
}