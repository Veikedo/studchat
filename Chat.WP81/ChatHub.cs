﻿using System;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Popups;
using Chat.WP81.Models;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Transports;

namespace Chat.WP81
{
	public interface IChatHub
	{
		IObservable<Message> OnMessageReceived { get; }
		IObservable<Conversation> OnDialogStarted { get; }
		IObservable<Conversation> OnDialogDied { get; }
		Task StartDialogAsync();
		Task SendMessageAsync(Guid dialogId, string message);
	}

	public class Conversation
	{
		public Conversation(Guid conversationId)
		{
			ConversationId = conversationId;
		}

		public Guid ConversationId { get; }
	}

	public class ChatHub : IChatHub
	{
		private readonly IHubProxy _chatHub;
		private readonly HubConnection _hubConnection;
		private readonly ISubject<Conversation> _onDialogDied = new Subject<Conversation>();
		private readonly ISubject<Conversation> _onDialogStarted = new Subject<Conversation>();
		private readonly ISubject<Message> _onMessageReceived = new Subject<Message>();
		private ConnectionState _connectionState;

		public ChatHub()
		{
			_hubConnection = new HubConnection("http://10.0.0.217/Chat.WebApp/");

			_hubConnection.StateChanged += change => _connectionState = change.NewState;
			_hubConnection.Error +=  e =>
			{
			};

			_chatHub = _hubConnection.CreateHubProxy("ChatHub");
			_chatHub.On<Message>("OnMessageReceivedAsync", m => _onMessageReceived.OnNext(m));
			_chatHub.On<Guid>("OnDialogStartedAsync", id => _onDialogStarted.OnNext(new Conversation(id)));
			_chatHub.On<Guid>("OnDialogDiedAsync", id => _onDialogDied.OnNext(new Conversation(id)));
		}

		public IObservable<Message> OnMessageReceived => _onMessageReceived;
		public IObservable<Conversation> OnDialogStarted => _onDialogStarted;
		public IObservable<Conversation> OnDialogDied => _onDialogDied;

		public async Task StartDialogAsync()
		{
			if (_connectionState != ConnectionState.Connected)
			{
				await _hubConnection.Start(new WebSocketTransport()).ConfigureAwait(false);
			}

			await _chatHub.Invoke("StartDialogAsync").ConfigureAwait(false);
		}

		public async Task SendMessageAsync(Guid dialogId, string message)
		{
			if (_connectionState == ConnectionState.Disconnected)
			{
				await _hubConnection.Start().ConfigureAwait(false);
			}

			await _chatHub.Invoke("SendMessageAsync", dialogId, message).ConfigureAwait(false);
		}
	}
}