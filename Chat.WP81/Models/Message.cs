﻿using System;

namespace Chat.WP81.Models
{
	public class Message
	{
		public Message(Guid conversationId, string message, bool isMyMessage)
		{
			ConversationId = conversationId;
			Content = message;
			IsMyMessage = isMyMessage;
		}

		public Guid ConversationId { get; set; }
		public string Content { get; set; }
		public bool IsMyMessage { get; set; }

	}
}