﻿namespace Chat.WP81.Helpers
{
	public static class Extensions
	{
		public static bool IsNullOrEmpty(this string str)
		{
			return string.IsNullOrEmpty(str);
		}
	}
}