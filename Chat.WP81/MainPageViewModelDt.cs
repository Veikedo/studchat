using ReactiveUI;

namespace Chat.WP81
{
	public class MainPageViewModelDt : MainPageViewModel
	{
		public MainPageViewModelDt()
		{
			Messages = new ReactiveList<MessageViewModel>
			{
				new MessageViewModel("Hello"),
				new MessageViewModel("Lopem ipsum dolor sit amet"),
				new MessageViewModel("Weather is fine today"),
				new MessageViewModel("How are you?"),
			};
		}
	}
}