using System;
using System.Threading.Tasks;

namespace Chat.WebApp.Hubs
{
	public interface IChatClient
	{
		Task OnDialogStartedAsync(Guid conversationId);
		Task OnDialogDiedAsync(Guid conversationId);

		/// <summary>
		/// </summary>
		/// <param name="conversationId"></param>
		/// <param name="message"></param>
		/// <param name="isUserMessage">Is this message was sent by this user</param>
		Task OnMessageReceivedAsync(Message message);
	}
}