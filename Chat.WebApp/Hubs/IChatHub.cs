using System;
using System.Threading.Tasks;

namespace Chat.WebApp.Hubs
{
    /// <summary>
    /// </summary>
    public interface IDialogSearchParams
    {
    }

    /// <summary>
    /// </summary>
    public interface IInterlocutorDescription
    {
        Gender Gender { get; }
    }

    public enum Gender
    {
        Male,
        Female
    }

    public interface IChatHub
    {
        Task StartDialogAsync();
        Task SendMessageAsync(Guid dialogId, string text);
    }
}