using System;
using Newtonsoft.Json;

namespace Chat.WebApp.Hubs
{
	public class Message
	{
		public Message(Guid conversationId, string message, bool isUserMessage)
		{
			ConversationId = conversationId;
			Content = message;
			IsUserMessage = isUserMessage;
		}

		public Guid ConversationId { get; }

		public string Content { get; }

		[JsonProperty("isMyMessage")]
		public bool IsUserMessage { get; }
	}
}