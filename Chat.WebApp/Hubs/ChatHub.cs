﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chat.WebApp.Helpers;
using Microsoft.AspNet.SignalR;
using Nito.AsyncEx;

namespace Chat.WebApp.Hubs
{
	internal class Dialog
	{
		private readonly List<string> _userIds;

		public Dialog()
		{
			DialogId = Guid.NewGuid();
			_userIds = new List<string>();
		}

		public Guid DialogId { get; }
		public IReadOnlyCollection<string> UserIds => _userIds;

		public void AddUsers(params string[] userIds)
		{
			_userIds.AddRange(userIds);
		}
	}

	public class ChatHub : Hub<IChatClient>, IChatHub
	{
		private static readonly AsyncLock Mutex = new AsyncLock();

		/// <summary>
		///     active dialogs
		/// </summary>
		private static readonly Dictionary<Guid, Dialog> StartedDialogs = new Dictionary<Guid, Dialog>();

		/// <summary>
		///     Users waiting to dialog starting
		/// </summary>
		private static readonly List<string> WaitingUserIds = new List<string>();

		/// <summary>
		///     Mapping user to their dialogs
		///     Key is user connection id
		/// </summary>
		private static readonly Dictionary<string, Dialog> UserDialogs = new Dictionary<string, Dialog>();

		public async Task StartDialogAsync()
		{
            using (await Mutex.LockAsync().ConfigureAwait(false))
			{
				if (WaitingUserIds.Any(x => x == Context.ConnectionId))
				{
					return;
				}

				// if user was participating in dialog, then kick him from it and notify other user
				await OnUserLeftConversationAsync(Context.ConnectionId).ConfigureAwait(false);
                
				var userId = WaitingUserIds.FirstOrDefault();
				if (userId != null)
				{
					// if there is a already waiting user then start the dialog
					var dialog = new Dialog();
					dialog.AddUsers(userId, Context.ConnectionId);

					StartedDialogs.Add(dialog.DialogId, dialog);
					UserDialogs.Add(userId, dialog);
					UserDialogs.Add(Context.ConnectionId, dialog);

					WaitingUserIds.Remove(userId);

					await GetClients(dialog.UserIds).OnDialogStartedAsync(dialog.DialogId).ConfigureAwait(false);
				}
				else
				{
					WaitingUserIds.Add(Context.ConnectionId);
				}
			}
		}

		public async Task SendMessageAsync(Guid dialogId, string text)
		{
			using (await Mutex.LockAsync().ConfigureAwait(false))
			{
				var dialog = StartedDialogs.GetValueOrDefault(dialogId);
				if (dialog != null)
				{
					var connectionIds = dialog.UserIds;
					foreach (var connectionId in connectionIds)
					{
						var message = new Message(dialogId, text, isUserMessage: connectionId == Context.ConnectionId);
						await Clients.Client(connectionId).OnMessageReceivedAsync(message).ConfigureAwait(false);
					}
				}
			}
		}

		public override async Task OnDisconnected(bool stopCalled)
		{
			using (await Mutex.LockAsync().ConfigureAwait(false))
			{
				if (WaitingUserIds.Remove(Context.ConnectionId))
				{
					// if user was in the waiting queue, then he was not participating in any dialogs
					return;
				}

				await OnUserLeftConversationAsync(Context.ConnectionId).ConfigureAwait(false);
			}
		}

		private async Task OnUserLeftConversationAsync(string userId)
		{
			var userDialog = UserDialogs.GetValueOrDefault(userId);
			if (userDialog != null)
			{
				var userToNotify = userDialog.UserIds.FirstOrDefault(x => x != userId);
				if (userToNotify != null)
				{
					await Clients.Client(userToNotify).OnDialogDiedAsync(userDialog.DialogId).ConfigureAwait(false);
				}

				StartedDialogs.Remove(userDialog.DialogId);
				UserDialogs.RemoveRange(userDialog.UserIds);
			}
		}

	    private IChatClient GetClients(IReadOnlyCollection<string> userIds) => Clients.Clients(userIds.ToList());
	}
}