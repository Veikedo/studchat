﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Chat.WebApp.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Chat.WebApp.Controllers
{
	public class HomeController : Controller
	{
		private readonly AppUserManager _appUserManager;
		private readonly ApplicationSignInManager _signInManager;

		public HomeController(AppUserManager appUserManager, ApplicationSignInManager signInManager)
		{
			_appUserManager = appUserManager;
			_signInManager = signInManager;
		}

		public HomeController()
		{
		}

		private ApplicationSignInManager SignInManager
			=> _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();

		private AppUserManager AppUserManager
			=> _appUserManager ?? HttpContext.GetOwinContext().GetUserManager<AppUserManager>();

		public ActionResult Index()
		{
			/*
			if (!User.Identity.IsAuthenticated)
			{
				var user = new IdentityUser {UserName = Guid.NewGuid().ToString("N")};
				var result = await AppUserManager.CreateAsync(user, "passw").ConfigureAwait(false);

				if (result.Succeeded)
				{
					await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false).ConfigureAwait(false);
					return RedirectToAction("Index", "Home");
				}
			}*/

			return View();
		}
		public ViewResult Test()
		{
			return View();
		}
	}
}