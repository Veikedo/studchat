﻿function Message(type, content) {
    this.messageType = type;
    this.content = content;
}

var MessageType = {
    ServiceMessage: 0,
    ChatMessage: 1,
    Warning: 2,
    Success: 3,
    Info: 4,
    MyMessage: 5,
    HisMessage: 6
};

var ConversationStatus = {
    NotStarted: 0,
    WaitingForConnection: 1,
    Started: 2
};

if (Object.freeze) {
    Object.freeze(ConversationStatus);
    Object.freeze(MessageType);
}

function ChatViewModel() {
    var self = this;

    function pushMessage(type, content) {
        var message = new Message(type, fromHtmlEntities(content));
        self.messages.push(message);
    }

    self.chatHub = $.connection.chatHub;
    self.messageContent = ko.observable("");
    self.conversationStatus = ko.observable(ConversationStatus.NotStarted);
    self.messages = ko.observableArray().extend({ scrollFollow: "#messageList" });
    self.isMessageInputFocused = ko.observable(false);
    self.conversationId = null;

    self.startNewConversation = function () {
        if ($.connection.hub.state === $.signalR.connectionState.disconnected) {
            self.initChatHub();
        } else if (self.conversationStatus() !== ConversationStatus.WaitingForConnection) {
            self.messages.removeAll();

            self.chatHub.server.startDialogAsync();
            self.conversationStatus(ConversationStatus.WaitingForConnection);

            pushMessage(MessageType.ServiceMessage, "Ожидаем собеседника..");
        }
    };

    self.chatHub.client.onDialogStartedAsync = function (conversationId) {
        self.conversationId = conversationId;
        self.conversationStatus(ConversationStatus.Started);

        pushMessage(MessageType.ServiceMessage, "Собеседник найден. Скажи «Привет!»");
    };

    self.sendMessage = function () {
        if (self.conversationStatus() === ConversationStatus.Started && self.messageContent().trim().length > 0) {
            var message = self.messageContent();
            self.chatHub.server.sendMessageAsync(self.conversationId, message);
            self.messageContent("");
            self.isMessageInputFocused(true);
        }
    };

    self.chatHub.client.onMessageReceivedAsync = function (message) {
        pushMessage(message.isMyMessage ? MessageType.MyMessage : MessageType.HisMessage, message.content);
    };

    self.chatHub.client.onDialogDiedAsync = function (conversationId) {
        self.conversationStatus(ConversationStatus.NotStarted);
        pushMessage(MessageType.ServiceMessage, "Собеседник отключился. Нажми «Новый диалог» вверху страницы, чтобы найти другого");
        // self.startNewConversation();
    };

    $(window).unload(function () {
        if ($.connection.hub && $.connection.hub.state !== $.signalR.connectionState.disconnected) {
            $.connection.hub.stop();
        }
    });

    self.initChatHub = function () {
        $.connection.hub.start()
            .done(self.startNewConversation)
            .fail(function (e) { alert(e) });
    };

    self.initChatHub();
}

$(function () {
    ko.applyBindings(new ChatViewModel());
});


function fromHtmlEntities(text) {
    return (text + "").replace(/&#\d+;/gm, function (s) {
        return String.fromCharCode(s.match(/\d+/gm)[0]);
    });
}

ko.bindingHandlers.onenter = {
    init: function (element, valueAccessor) {
        var f = valueAccessor();
        $(element).keydown(function (e) {
            if (e.keyCode === 13) {
                f();
            }
        });
    }
};

ko.extenders.scrollFollow = function (target, selector) {
    target.subscribe(function (newval) {
        var el = document.querySelector(selector);

        // the scroll bar is all the way down, so we know they want to follow the text
        if (el.scrollTop === el.scrollHeight - el.clientHeight) {
            // have to push our code outside of this thread since the text hasn't updated yet
            setTimeout(function () { el.scrollTop = el.scrollHeight - el.clientHeight; }, 0);
        }
    });

    return target;
};
