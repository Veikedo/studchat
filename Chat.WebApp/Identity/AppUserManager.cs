﻿using System;
using System.Web.Mvc;
using Chat.Dal;
using Chat.WebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Ninject;

namespace Chat.WebApp.Identity
{
	public class AppUserManager : UserManager<IdentityUser, Guid>
	{
		public AppUserManager(IUserStore<IdentityUser, Guid> store) : base(store)
		{
		}

		public static AppUserManager Create(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
		{
			var kernel = context.Get<IKernel>();
			var manager = new AppUserManager(kernel.Get<IUserStore<IdentityUser, Guid>>());

			manager.UserValidator = new UserValidator<IdentityUser, Guid>(manager);
			manager.PasswordValidator = new PasswordValidator {RequiredLength = 0};

			var dataProtectionProvider = options.DataProtectionProvider;
			if (dataProtectionProvider != null)
			{
				var dataProtector = dataProtectionProvider.Create("ASP.NET Identity");
				manager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser, Guid>(dataProtector);
			}

			return manager;
		}
	}
}