using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Chat.WebApp.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Chat.WebApp.Identity
{
	public class ApplicationSignInManager : SignInManager<IdentityUser, Guid>
	{
		public ApplicationSignInManager(AppUserManager appUserManager, IAuthenticationManager authenticationManager)
			: base(appUserManager, authenticationManager)
		{
		}

		public override Task<ClaimsIdentity> CreateUserIdentityAsync(IdentityUser identityUser)
		{
			return identityUser.GenerateUserIdentityAsync(UserManager);
		}

		public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
		{
			return new ApplicationSignInManager(context.GetUserManager<AppUserManager>(), context.Authentication);
		}
	}
}