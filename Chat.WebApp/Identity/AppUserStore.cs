using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Chat.Dal;
using Chat.WebApp.Helpers;
using Chat.WebApp.Models;
using Microsoft.AspNet.Identity;

namespace Chat.WebApp.Identity
{
	public class AppUserStore : IUserPasswordStore<IdentityUser, Guid>
	{
		private readonly IMapper _mapper;
		private readonly IServiceProvider _serviceProvider;

		public AppUserStore(IServiceProvider serviceProvider)
		{
			_serviceProvider = serviceProvider;
			_mapper = serviceProvider.Get<IMapper>();
		}

		public void Dispose()
		{
		}

		public async Task CreateAsync(IdentityUser identityUser)
		{
			var user = _mapper.Map<IdentityUser, User>(identityUser);
			using (var context = _serviceProvider.Get<IAppDbContext>())
			{
				context.Users.Add(user);
				await context.SaveChangesAsync().ConfigureAwait(false);
			}
		}

		public async Task UpdateAsync(IdentityUser identityUser)
		{
			var user = _mapper.Map<IdentityUser, User>(identityUser);

			using (var context = _serviceProvider.Get<IAppDbContext>())
			{
				context.MarkAsModified(user);
				await context.SaveChangesAsync().ConfigureAwait(false);
			}
		}

		public async Task DeleteAsync(IdentityUser identityUser)
		{
			using (var context = _serviceProvider.Get<IAppDbContext>())
			{
				// TODO optimize work
				var user = await context.Users.FirstOrDefaultAsync(x => x.Id == identityUser.Id).ConfigureAwait(false);
				context.Users.Remove(user);

				await context.SaveChangesAsync().ConfigureAwait(false);
			}
		}

		public async Task<IdentityUser> FindByIdAsync(Guid userId)
		{
			using (var context = _serviceProvider.Get<IAppDbContext>())
			{
				var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
				return user == null ? null : _mapper.Map<User, IdentityUser>(user);
			}
		}

		public async Task<IdentityUser> FindByNameAsync(string userName)
		{
			using (var context = _serviceProvider.Get<IAppDbContext>())
			{
				var user = await context.Users.FirstOrDefaultAsync(x => x.UserName == userName).ConfigureAwait(false);
				return user == null ? null : _mapper.Map<User, IdentityUser>(user);
			}
		}

		public Task SetPasswordHashAsync(IdentityUser user, string passwordHash)
		{
			return Task.FromResult(0);
		}

		public Task<string> GetPasswordHashAsync(IdentityUser user)
		{
			return Task.FromResult("passw");
		}

		public Task<bool> HasPasswordAsync(IdentityUser user)
		{
			return Task.FromResult(true);
		}
	}
}