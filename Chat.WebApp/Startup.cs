﻿using System;
using Chat.WebApp;
using Chat.WebApp.Helpers;
using Chat.WebApp.Hubs;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Newtonsoft.Json;
using Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace Chat.WebApp
{
	public partial class Startup
	{
		private static readonly Lazy<JsonSerializer> JsonSerializerFactory;

		static Startup()
		{
			JsonSerializerFactory = new Lazy<JsonSerializer>(() => new JsonSerializer
			{
				ContractResolver = new FilteredCamelCasePropertyNamesContractResolver
				{
					TypesToInclude = {typeof (Message)}
				}
			});
		}

		public void Configuration(IAppBuilder app)
		{
			ConfigureAuth(app);
			app.MapSignalR();

			GlobalHost.DependencyResolver.Register(typeof (JsonSerializer), () => JsonSerializerFactory.Value);
		}
	}
}