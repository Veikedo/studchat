﻿using System;
using System.Web.Mvc;

namespace Chat.WebApp.Helpers
{
	public class ServiceProvider : IServiceProvider
	{
		public object GetService(Type serviceType)
		{
			return DependencyResolver.Current.GetService(serviceType);
		}
	}
}