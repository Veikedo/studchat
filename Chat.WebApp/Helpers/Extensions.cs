﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Chat.WebApp.Helpers
{
	public static class Extensions
	{
		public static T Get<T>(this IServiceProvider serviceProvider)
		{
			return (T) serviceProvider.GetService(typeof (T));
		}

		public static TValue GetValueOrDefault<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
		{
			TValue value;
			return dictionary.TryGetValue(key, out value) ? value : default(TValue);
		}

		public static int RemoveRange<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<TKey> keys)
		{
			return keys.Count(dictionary.Remove);
		}

		public static string ToCamelCase(this string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return value;
			}
			var firstChar = value[0];
			if (char.IsLower(firstChar))
			{
				return value;
			}
			firstChar = char.ToLowerInvariant(firstChar);
			return firstChar + value.Substring(1);
		}
	}
}