using System;
using System.Web;
using Chat.Dal;
using Chat.WebApp.App_Start;
using Chat.WebApp.Helpers;
using Chat.WebApp.Identity;
using Chat.WebApp.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace Chat.WebApp.App_Start
{
	public static class NinjectWebCommon
	{
		private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

		/// <summary>
		///     Starts the application
		/// </summary>
		public static void Start()
		{
			DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
			DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
			Bootstrapper.Initialize(CreateKernel);
		}

		/// <summary>
		///     Stops the application.
		/// </summary>
		public static void Stop()
		{
			Bootstrapper.ShutDown();
		}

		/// <summary>
		///     Creates the kernel that will manage your application.
		/// </summary>
		/// <returns>The created kernel.</returns>
		public static IKernel CreateKernel()
		{
			var kernel = new StandardKernel();

			try
			{
				kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
				kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

				RegisterServices(kernel);
				return kernel;
			}
			catch
			{
				kernel.Dispose();
				throw;
			}
		}

		/// <summary>
		///     Load your modules or register your services here!
		/// </summary>
		/// <param name="kernel">The kernel.</param>
		private static void RegisterServices(IKernel kernel)
		{
			kernel.Bind<IAppDbContext>().To<AppDbContext>().InTransientScope();
			kernel.Bind<IMapper>().To<CommonMapper>().InTransientScope();
			// kernel.Bind<IConversationManager>().To<ConversationManager>().InSingletonScope();
			kernel.Bind<IUserStore<IdentityUser, Guid>>().To<AppUserStore>().InTransientScope();

			//kernel.Bind<IHubConnectionContext<IChatClient>>()
			//.ToMethod(_ => GlobalHost.ConnectionManager.GetHubContext<ChatHub, IChatClient>().Clients);

			kernel.Bind<IServiceProvider>().To<ServiceProvider>().InTransientScope();
		}
	}
}