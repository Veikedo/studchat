﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace Chat.WebApp.Models
{
	public class IdentityUser : IUser<Guid>
	{
		public Guid Id { get; set; }
		public string UserName { get; set; }

		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<IdentityUser, Guid> manager)
		{
			// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
			var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie)
				                         .ConfigureAwait(false);

			var user = await manager.FindByNameAsync(UserName).ConfigureAwait(false);
			if (user != null)
			{
				Id = user.Id;
				userIdentity.AddClaim(new Claim("myUserId", Id.ToString("N")));
			}

			return userIdentity;
		}
	}
}